FROM python:3.7-slim

RUN apt-get update && apt-get install build-essential -y

# copy the requirements file into the image
COPY ./req.txt /app/requirements.txt

# switch working directory
WORKDIR /app

EXPOSE 5000

# install the dependencies and packages in the requirements file
RUN pip install -r requirements.txt

# copy every content from the local file to the image
COPY . /app

# configure the container to run in an executed manner
ENTRYPOINT [ "python" ]

CMD ["app.py"]